/**
 * PM2 application configuration
 * http://pm2.keymetrics.io/docs/usage/application-declaration/
 * NODE_ENV=production PORT=8822 pm2 startOrReload ecosystem.config.js --update-env
 */
module.exports = {
  apps: [
    {
      script: "npm",
      args: ["run", "production"],
      name: `slack-app`,
      merge_logs: true,
      log_date_format: "YYYY-MM-DD HH:mm:ss",
      autorestart: true,
    },
  ],
}
