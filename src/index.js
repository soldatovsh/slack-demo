const restify = require("restify")
const { WebClient } = require("@slack/web-api")
const axios = require("axios")

const PORT = 8822
const TOKEN = "xoxb-3319647677463-3346884210113-cPQDek5Kip2pF43kFSq3JJga"
const CHANNEL = "C039UBUU4TV"

const OPTIONS = [
  {
    text: {
      type: "plain_text",
      text: "ошибка v1",
    },
    value: "v1",
  },
  {
    text: {
      type: "plain_text",
      text: "ошибка v2",
    },
    value: "v2",
  },
  {
    text: {
      type: "plain_text",
      text: "Все плохо",
    },
    value: "v1/v2",
  },
]

const web = new WebClient(TOKEN)

const server = restify.createServer()
server.use(restify.plugins.bodyParser())

// Обработка кнопок окна
// https://api.slack.com/surfaces/modals/using#displaying_errors
server.post("/api/inter", async (req, res) => {
  try {
    const data = JSON.parse(req.body.payload)
    console.log("----")

    if (data.type === "block_actions") {
      const attachments = data.message.attachments
      data.message.attachments[0].blocks[1] = {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `🔨 взял в работу: <@${data.user.id}>`,
        },
      }
      return await axios.post(data.response_url, {
        attachments,
      })
    } else if (data.type === "view_submission") {
      const ids = data.view.blocks.map((i) => i.block_id)
      // console.log(JSON.stringify(data.view.state, null, 2))

      const result = await web.chat.postMessage({
        channel: CHANNEL,
        attachments: [
          {
            color: "#f2c744",
            blocks: [
              {
                type: "section",
                text: {
                  type: "mrkdwn",
                  text: `
🚀 *[${
                    data.view.state.values[ids[1]]["system-variant"]
                      .selected_option.value
                  }]* ${data.view.state.values[ids[0]]["system-title"].value}\n
🚀 описание: ${data.view.state.values[ids[2]]["system-text"].value}\n
🚀 автор: <@${data.user.id}>`,
                },
              },
              {
                type: "actions",
                elements: [
                  {
                    type: "button",
                    text: {
                      type: "plain_text",
                      text: "Взять в работу",
                      emoji: true,
                    },
                    value: "click_me_123",
                    action_id: "to_work",
                  },
                ],
              },
            ],
          },
        ],
      })

      web.chat.postMessage()
    }
  } catch (e) {
    console.error(e)
  }

  // return res.status(200)
  return res.json({
    response_action: "clear",
  })
})

server.post("/api/report", async (req, res) => {
  // console.log(req.body)

  const { text, trigger_id } = req.body

  try {
    const modal = await web.views.open({
      trigger_id: trigger_id,
      view: {
        type: "modal",
        callback_id: "view_identifier",
        title: {
          type: "plain_text",
          text: "Создать новый тикет",
        },
        submit: {
          type: "plain_text",
          text: "Отправить",
        },
        blocks: [
          {
            type: "input",
            label: {
              type: "plain_text",
              text: "Краткое описание",
              emoji: true,
            },
            element: {
              type: "plain_text_input",
              initial_value: text,
              focus_on_load: true,
              action_id: "system-title",
            },
          },
          {
            type: "input",
            label: {
              type: "plain_text",
              text: "В какой среде ошибка",
            },
            element: {
              type: "static_select",
              action_id: "system-variant",
              initial_option: OPTIONS[0],

              placeholder: {
                type: "plain_text",
                text: "Версия системы",
                emoji: true,
              },
              options: OPTIONS,
            },
          },
          {
            type: "input",
            label: {
              type: "plain_text",
              text: "Подробно",
              emoji: true,
            },
            element: {
              type: "plain_text_input",
              multiline: true,
              action_id: "system-text",
            },
          },
        ],
      },
    })
  } catch (e) {
    //   console.error(e)
  }

  // // The result contains an identifier for the message, `ts`.
  // console.log(
  //   `Successfully send message ${result.ts} in conversation ${conversationId}`
  // )

  // return res.json("ok")
  return res.json()
})

server.listen(PORT, function () {
  console.log("%s listening at %s", server.name, server.url)
})
